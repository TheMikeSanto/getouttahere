#!/bin/bash
SSH_USER=netbelvitz_getouttahere@ssh.phx.nearlyfreespeech.net
SSH_COMMAND="cd /home/public/;\
  rm -rf {countdown_component, index.html, style.css, *.gif}; \
  tar xvzf -"

function deployMsg {
  printf "\n  !=> $1\n"
}

deployMsg "Building dist"
mkdir -p .dist
rsync -av --exclude="*.js" src/ .dist/
rsync -av favicons/* .dist/
cp .htaccess .dist/
uglifyjs \
  --ecma 6 \
  --compress \
  --mangle \
  --verbose \
  src/countdown_component/countdown.js \
  -o .dist/countdown_component/countdown.js
deployMsg "Compressing"
cd .dist && tar -cvzf ../bundle.tar.gz . && cd -
deployMsg "Deploying to server"
cat bundle.tar.gz | ssh $SSH_USER -C $SSH_COMMAND
deployMsg "Tidying up"
rm -rf bundle.tar.gz .dist/
deployMsg "Deployed, sucka!"