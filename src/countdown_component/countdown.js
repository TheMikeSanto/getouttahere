'use strict';

const currentDocument = document.currentScript.ownerDocument;

class ShooCountDown extends HTMLElement {

  static get defaults() {
    return {
      count: 5,
      redirectTo: "https://www.google.com",
    };
  }
  // these domains cause an infinite loop and must be stopped
  static get referrerBlacklist() {
    return [
      "bit.ly",
      "goo.gl",
      "is.gd",
      "lc.chat",
      "s2r.co",
      "soo.gd",
      "t.co",
      "tiny.cc",
      "tinyurl.com",
    ];
  }
  // rate of countdown in milliseconds
  static get interval() { return 1000; }

  constructor() {
    super();
  }

  connectedCallback() {
    const shadowRoot = this.attachShadow({mode: 'open'});
    const template = currentDocument.querySelector('#countdown-template');
    const instance = template.content.cloneNode(true);
    shadowRoot.appendChild(instance);
    this.contentArea = this.shadowRoot.querySelector('h1');
    this.doCountdown(this.startingCount)
      .then(() => this.shoo());
  }

  doCountdown(count) {
    this.renderCount(count);
    return new Promise(resolve => {
      const interval = setInterval(() => {
        this.renderCount(--count);
        if (!count) {
          window.clearInterval(interval);
          resolve();
        }
      }, ShooCountDown.interval);
    });
  }

  get referrerHostname() {
    const localHostname = document.location.hostname;
    let a = currentDocument.createElement('a');
    a.href = document.referrer;
    return a.hostname !== localHostname ? a.hostname : "";
  }

  get redirectTo() {
    return ShooCountDown.referrerBlacklist.includes(this.referrerHostname)
      ? ShooCountDown.defaults.redirectTo
      : document.referrer || ShooCountDown.defaults.redirectTo
  }

  get startingCount() {
    return Number.parseInt(this.getAttribute('count'))
      || ShooCountDown.defaults.count;
  }

  renderCount (count) {
    this.contentArea.innerHTML = count;
  }

  shoo () {
    this.contentArea.innerHTML = 'GET OUTTA HERE';
    this.contentArea.style.setProperty('font-size', '15em');
    this.contentArea.style.setProperty('margin-top', '0.5em');
    this.contentArea.style.setProperty('opacity', '1');
    setTimeout(() => {
      window.location.href = this.redirectTo;
    }, ShooCountDown.interval)
  }
}

customElements.define('shoo-countdown', ShooCountDown);
